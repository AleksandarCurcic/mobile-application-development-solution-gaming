package com.example.solution_gaming;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.solution_gaming.Database;

public class MainActivity extends AppCompatActivity {

    Database database = new Database(this);
    EditText loginIme, loginLozinka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonPrijaviSe = findViewById(R.id.buttonPrijaviSe);
        Button buttonRegistrujSe = findViewById(R.id.buttonRegistrujSe);


        buttonPrijaviSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginIme = findViewById(R.id.loginIme);
                loginLozinka = findViewById(R.id.loginLozinka);

                String ime = loginIme.getText().toString();
                String lozinka = loginLozinka.getText().toString();

                if(ime.equals("")||lozinka.equals(""))
                    Toast.makeText(MainActivity.this, "Popunite sva polja!", Toast.LENGTH_SHORT).show();
                else{
                    Boolean proveraPodataka = database.proveraKorImenaILozinke(ime, lozinka);
                    if(proveraPodataka==true){
                        Toast.makeText(MainActivity.this, "Uspesna prijava!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), PocetnaActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Korisnicko ime ili lozinka nisu ispravni!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        buttonRegistrujSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistracijaActivity.class);
                startActivity(intent);
            }
        });
    }

}