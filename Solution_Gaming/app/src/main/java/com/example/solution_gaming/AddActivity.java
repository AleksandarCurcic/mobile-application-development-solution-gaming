package com.example.solution_gaming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.solution_gaming.Database;

public class AddActivity extends AppCompatActivity {

    Database database = new Database(this);
    EditText zanr, naziv, cena;
    Button dugmeAdd, dugmeOdustani;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);


        naziv = findViewById(R.id.naziv);
        zanr = findViewById(R.id.zanr);
        cena = findViewById(R.id.cena);
        dugmeAdd = findViewById(R.id.dugmeAdd);
        dugmeOdustani = findViewById(R.id.dugmeOdustani);

        dugmeOdustani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddActivity.this, PocetnaActivity.class);
                startActivity(intent);
                finish();
            }
        });

        dugmeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String stringNaziv = naziv.getText().toString();
                String stringZanr = zanr.getText().toString();
                String stringCena = cena.getText().toString();
                Integer intCena = Integer.parseInt(stringCena);

                if (stringNaziv.equals("") || stringZanr.equals("") || stringCena.equals(""))
                    Toast.makeText(AddActivity.this, "Popunite sva polja!", Toast.LENGTH_SHORT).show();
                else {
                    Igra igra = new Igra();
                    igra.setNaziv(stringNaziv);
                    igra.setZanr(stringZanr);
                    igra.setCena(intCena);
                    database.addIgra(igra);
                    Toast.makeText(AddActivity.this, "Igra uspesno dodat!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AddActivity.this, PocetnaActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}