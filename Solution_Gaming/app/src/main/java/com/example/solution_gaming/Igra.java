package com.example.solution_gaming;

public class Igra {

    private Integer id;
    private String naziv;
    private String zanr;
    private Integer cena;

    public Igra() {
    }

    public Igra(Integer id, String naziv, String zanr, Integer cena) {
        this.id = id;
        this.naziv = naziv;
        this.zanr = zanr;
        this.cena = cena;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getZanr() {
        return zanr;
    }
    public void setZanr(String zanr) {
        this.zanr = zanr;
    }

    public Integer getCena() {
        return cena;
    }
    public void setCena(Integer cena) {
        this.cena = cena;
    }
}
