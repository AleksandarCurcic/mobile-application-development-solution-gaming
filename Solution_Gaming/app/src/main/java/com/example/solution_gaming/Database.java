package com.example.solution_gaming;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.solution_gaming.Igra;
import com.example.solution_gaming.Korisnik;


public class Database extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "korisnici.db";
    private static final String TABLE_NAME = "korisnici";
    private static final String TABLE_NAME2 = "igre";
    SQLiteDatabase db;

    public Database(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, korisnicko_ime TEXT, lozinka TEXT)";
        db.execSQL(query);
        String query2 = "CREATE TABLE " + TABLE_NAME2 + "(id INTEGER PRIMARY KEY AUTOINCREMENT, naziv TEXT, zanr TEXT, cena INTEGER)";
        db.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(query);
        String query2 = "DROP TABLE IF EXISTS " + TABLE_NAME2;
        db.execSQL(query2);
        onCreate(db);
    }

    public void addKorisnik(Korisnik korisnik){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("korisnicko_ime", korisnik.getKorisnickoIme());
        values.put("lozinka", korisnik.getSifra());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public Boolean proveraKorImena(String ime) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM korisnici WHERE korisnicko_ime = ?", new String[] {ime});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }

    public Boolean proveraKorImenaILozinke(String ime, String lozinka) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM korisnici WHERE korisnicko_ime = ? and lozinka = ?", new String[] {ime,lozinka});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }



    public void addIgra(Igra igra){
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("naziv", igra.getNaziv());
        contentValues.put("zanr", igra.getZanr());
        contentValues.put("cena", igra.getCena());
        db.insert("igre", null, contentValues);
        db.close();
    }

    public void editIgra (Igra igra){
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("naziv", igra.getNaziv());
        contentValues.put("zanr", igra.getZanr());
        contentValues.put("cena", igra.getCena());
        db.update("igre", contentValues, "id=" + "=?", new String[] { String.valueOf(igra.getId()) });
    }
}
