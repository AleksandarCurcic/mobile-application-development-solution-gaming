package com.example.solution_gaming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.solution_gaming.Database;

public class EditActivity extends AppCompatActivity {

    Database database = new Database(this);
    EditText naziv,zanr,cena;
    Button  dugmeOdustani, dugmeEdit;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        naziv= findViewById(R.id.naziv2);
        zanr = findViewById(R.id.zanr2);
        cena = findViewById(R.id.cena2);
        dugmeOdustani = findViewById(R.id.dugmeOdustani2);
        dugmeEdit = findViewById(R.id.dugmeEdit2);

        dugmeOdustani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditActivity.this, PocetnaActivity.class);
                startActivity(intent);
                finish();
            }
        });


        dugmeEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String stringNaziv = naziv.getText().toString();
                String stringZanr = zanr.getText().toString();
                String stringCena = cena.getText().toString();
                Integer intCena = Integer.parseInt(stringCena);

                if(stringNaziv.equals("") || stringZanr.equals("") || stringCena.equals(""))
                    Toast.makeText(EditActivity.this, "Popunite sva polja!", Toast.LENGTH_SHORT).show();
                else {
                    Igra igra = new Igra();
                    igra.setId(id);
                    igra.setNaziv(stringNaziv);
                    igra.setZanr(stringZanr);
                    igra.setCena(intCena);

                    database.editIgra(igra);
                    Toast.makeText(EditActivity.this, "Igra je uspesno izmenjen!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(EditActivity.this, PocetnaActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        editdata();
    }

    private void editdata() {

        Bundle bundle = getIntent().getBundleExtra("igradata");
        id = bundle.getInt("id");
        naziv.setText(bundle.getString("naziv"));
        zanr.setText(bundle.getString("zanr"));
        cena.setText(String.valueOf(bundle.getInt("cena")));
    }

}