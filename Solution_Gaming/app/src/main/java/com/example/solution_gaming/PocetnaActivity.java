package com.example.solution_gaming;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.solution_gaming.Database;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


public class PocetnaActivity extends AppCompatActivity {

    Database database = new Database(this);
    SQLiteDatabase db;
    List<Igra> igre;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pocetna);

        FloatingActionButton dugmeFloatDodaj = findViewById(R.id.dugmeFloatAdd);
        listView = findViewById(R.id.listView);
        displaydata();

        dugmeFloatDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void displaydata() {
        db = database.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from igre", null);
        if (cursor.getCount() >= 0) {
            igre = new ArrayList<>();
            while (cursor.moveToNext()) {
                igre.add(new Igra(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getInt(3)));
            }

            CustAdapter custAdapter = new CustAdapter();
            listView.setAdapter(custAdapter);
        }
        else {
            Toast.makeText(this, "Lista je prazna, molimo vas da unesete igru!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), AddActivity.class);
            startActivity(intent);
            finish();
        }
    }


    private class CustAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return igre.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView txtNaziv, txtZanr, txtCena;
            ImageView icon_edit, icon_delete;

            convertView = LayoutInflater.from(PocetnaActivity.this).inflate(R.layout.listitem, parent, false);


            txtNaziv = convertView.findViewById(R.id.txt_naziv);
            txtZanr = convertView.findViewById(R.id.txt_zanr);
            txtCena = convertView.findViewById(R.id.txt_cena);
            icon_edit = convertView.findViewById(R.id.icon_edit);
            icon_delete = convertView.findViewById(R.id.icon_delete);

            txtNaziv.setText(igre.get(position).getNaziv());
            txtZanr.setText(igre.get(position).getZanr());
            //txtCena.setText(igre.get(position).getCena());
            txtCena.setText(String.valueOf(igre.get(position).getCena()));
            //JOS JEDNA PROMENLJIVA

            icon_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", igre.get(position).getId());
                    bundle.putString("naziv", igre.get(position).getNaziv());
                    bundle.putString("zanr", igre.get(position).getZanr());
                    bundle.putInt("cena",igre.get(position).getCena());

                    Intent intent = new Intent(PocetnaActivity.this, EditActivity.class);
                    intent.putExtra("igradata", bundle);
                    startActivity(intent);
                    finish();
                }
            });
            icon_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    db = database.getReadableDatabase();
                    db.delete("igre","id=" + igre.get(position).getId(), null);
                    Toast.makeText(PocetnaActivity.this, "Igra uspesno obrisan!", Toast.LENGTH_SHORT).show();
                    displaydata();
                }
            });
            return convertView;
        }
    }
}
