package com.example.solution_gaming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.solution_gaming.Database;

public class RegistracijaActivity extends AppCompatActivity {

    Database database = new Database(this);
    Korisnik korisnik = new Korisnik();
    EditText registracijaIme, registracijaLozinka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija);

        Button buttonRegistracija = findViewById(R.id.buttonRegistracija);
        Button buttonNazad = findViewById(R.id.buttonNazad);

        buttonRegistracija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registracijaIme = findViewById(R.id.registracijaIme);
                registracijaLozinka = findViewById(R.id.registracijaLozinka);

                String ime = registracijaIme.getText().toString();
                String lozinka = registracijaLozinka.getText().toString();


                if (ime.equals("") || lozinka.equals(""))
                    Toast.makeText(RegistracijaActivity.this, "Popunite sva polja!", Toast.LENGTH_SHORT).show();
                else {
                    Boolean proveraImena = database.proveraKorImena(ime);
                    if (proveraImena == false) {

                        korisnik.setKorisnickoIme(ime);
                        korisnik.setSifra(lozinka);
                        database.addKorisnik(korisnik);
                        Toast.makeText(RegistracijaActivity.this, "Uspesna registracija!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RegistracijaActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(RegistracijaActivity.this, "Korisnicko ime je zauzeto, pokusajte ponovo!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        buttonNazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistracijaActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}

